import { tommyman } from './elements/tommyman.js';
import './elements/body.js';
import './controls/audio.js';
import './controls/direction.js';
import './controls/pointer.js';
import './controls/speed.js';
import './helpers/position.js';
import './helpers/interval.js';
import './helpers/print.js';
import './helpers/random.js';

setTimeout(() => (tommyman.style.opacity = '1'), 0);
